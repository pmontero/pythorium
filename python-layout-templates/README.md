# Purpose
A collection of python-layout-templates use cases useful to start a new project.

# Reference

```bash
$ git remote -v
origin	git@gitlab.one.twcbiz.com:pmontero/python-layout-templates.git (fetch)
origin	git@gitlab.one.twcbiz.com:pmontero/python-layout-templates.git (push)
```

