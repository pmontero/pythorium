#!/usr/bin/env python
"""
__author__ = "Pedro Montero"
__copyright__ = "Copyright 2007, Oschestator Stack"
__credits__ = ["Rob Knight", "Peter Maxwell", "Gavin Huttley",
                    "Matthew Wakefield"]
__license__ = "GPL"
__version__ = "1.0.1"
__maintainer__ = "Rob Knight"
__email__ = "rob@spot.colorado.edu"
__status__ = "Production"

"""
# Importing libraries 

import os
import sys
import requests
# Importing packages and modules. Directory1 is a pacakge containing multiple modules.
from directory1 import module1
from directory1 import module2
