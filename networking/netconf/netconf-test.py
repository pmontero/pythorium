#! /usr/bin/python
"""
Testing network devices with netconf client
NAME         ADDRESS        DESCRIPTION  NED ID             ADMIN STATE
-----------------------------------------------------------------------
AUSDTXIRBZW  97.77.67.104   -            adva-xg116-nc-1.0  unlocked
AUSDTXIRDAW  71.42.150.181  -            adva-xg116-nc-1.0  unlocked

Usage:
Log the ouput
$ p2 netconf-test.py > netconf-test.py.logs
"""
import xmltodict
import xml.dom.minidom
from ncclient import manager

# Connecting to the device [open session]
m = manager.connect(host='71.42.150.181', port=830, username='root',
                    password='ChgMeNOW', hostkey_verify=False,
                    look_for_keys=False)
# device_params={'name': 'AUSDTXIRDAW'})

print m.connected

# Print capabilites
for c in m.server_capabilities:
    print c

# Get configuration
running_config = m.get_config('running')
# Print configuration in XML
print(xml.dom.minidom.parseString(str(running_config)).toprettyxml())
# Print configuration in JSON
conf_json = xmltodict.parse(str(running_config))
print conf_json
# conf_json['rpc-reply']["data"]["native"]["hostname"]

# Closing connection
m.close_session()