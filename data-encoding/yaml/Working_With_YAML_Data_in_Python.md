# Purpose

* Learning manipulating YAML formatted data

# References
[Python YAML tutorial](http://zetcode.com/python/yaml/)
[Creating Network Configuration with JINJA](https://routebythescript.com/creating-network-configurations-with-jinja/)
[Using YAML and JINJA to create network configurations](https://routebythescript.com/using-yaml-and-jinja-to-create-network-configurations/)
